import { __ } from '~/locale';

export const PREVENT_LEAVING_PENDING_REVIEW = __('There are unsubmitted review comments.');
